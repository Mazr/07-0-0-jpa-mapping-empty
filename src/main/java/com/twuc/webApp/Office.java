package com.twuc.webApp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Office {
    @Id
    private Long id;
    @Column
    private String city;

    public Office() {
    }

    public Office(long id, String city) {
        this.id = id;
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }
}
