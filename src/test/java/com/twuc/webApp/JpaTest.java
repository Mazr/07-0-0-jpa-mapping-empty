package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.annotation.DirtiesContext.MethodMode.BEFORE_METHOD;

@SuppressWarnings("SpringTestingDirtiesContextInspection")
@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = BEFORE_METHOD)
class JpaTest {
    @Autowired
    private OfficeRepository repository;

    @Autowired
    private EntityManager em;
    @Test
    void should_pass() {
        assertTrue(true);
    }

    @Test
    void should_save_entity() {
        Office entity = new Office(1, "xi'an");
        Office saveOffice = repository.save(entity);
        em.flush();
        em.clear();
        assertNotNull(saveOffice.getId());
    }

    @Test
    void should_use_em() {
        em.persist(new Office(1L,"chengdu"));
        em.flush();
        em.clear();
        Office office = em.find(Office.class, 1L);
        assertNotNull(office);
        assertEquals(Long.valueOf(1),office.getId());
        assertEquals("chengdu",office.getCity());

    }
}
